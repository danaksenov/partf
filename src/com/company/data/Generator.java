package com.company.data;
import com.company.model.phone;
import com.company.model.Store;

public final class Generator {
    private Generator() {
    }
    public static Store[] generate() {
        Store[] stores = new Store[3];
        {
            phone[] phone = {
                    new phone("Iphone", "X", 19600),
                    new phone("Samsung", "S20", 25000),
                    new phone("Huawei", "P40", 23100),
                    new phone("Xiaomi", "P30 Pro", 17900)
            };
            stores[0] = new Store("Comfy", "г.Харьков, ТЦ Французский Бульвар, ул. Академика Павлова, 44", phone);
        }
        {
            phone[] phone = {
                    new phone("Iphone", "X", 20100),
                    new phone("Samsung", "S20", 26100),
                    new phone("Huawei", "P40L", 9600),
                    new phone("Xiaomi", "P30 Pro", 16000),

            };
            stores[1] = new Store("Citrus", "вулиця Героїв Праці, 7, Харків", phone);
        }
        {
            phone[] phone = {
                    new phone("iPhone", "X", 21600),
                    new phone("Samsung", "S20", 28600),
                    new phone("Huawei", "P40", 19100),
                    new phone("Xiaomi", "P30 Pro", 18700)
            };
            stores[2] = new Store("Allo", "г.Харьков, вулиця Сумська, 39", phone);
        }

        return stores;
    }
}




