package com.company;

import com.company.data.Generator;
import com.company.model.StoreManager;

import java.util.Scanner;

public class Task {
    public void run() {
        StoreManager storeManager = new StoreManager(Generator.generate());


        Scanner scan = new Scanner(System.in);
        System.out.println("Выберите смартфон из перечня (Iphone, Samsung, Xiaomi, Huawei): ");
        String desiredManufacturer = scan.nextLine();
        System.out.println("Смартфон какой модели Вы хотите найти? (Iphone: X , 11 Pro ; Samsung:  S20 plus, M30S ; Huawei: P40, P40L; Xiaomi: P30 Pro, V20, P10): ");
        String desiredModel = scan.nextLine();

        storeManager.findAndPrintSmartphoneByManufacturer(desiredManufacturer);
        storeManager.findAndPrintCheapestSmartphoneByModel(desiredModel);
    }
}


