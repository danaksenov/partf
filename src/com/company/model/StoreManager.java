package com.company.model;

public class StoreManager {

    private final static int BIG_INT = Integer.MAX_VALUE;

    Store[] stores;

    public StoreManager(Store[] stores) {
        this.stores = stores;
    }

    public void findAndPrintCheapestSmartphoneByModel(String modelName) {
        int lowestPrice = BIG_INT;
        Store cheapestStore = getFirsrStoreWithFinderModel(modelName);
        if (cheapestStore == null) {
            System.err.println("Смартфона нет в наличии");
            return;
        }

        phone cheapSmartInShops = cheapestStore.cheapestSmartphoneByModel(BIG_INT, modelName);
        for (Store store : stores) {
            phone cheapestSmartInShop = store.cheapestSmartphoneByModel(lowestPrice, modelName);
            if (cheapestSmartInShop != null) {
                cheapSmartInShops = cheapestSmartInShop;
                cheapestStore = store;
                lowestPrice = cheapestSmartInShop.getPrice();
            }
        }
        PrintInformation.printCheapestSmartphoneByModel(cheapSmartInShops, cheapestStore);
    }

    private Store getFirsrStoreWithFinderModel(String modelName) {
        for (Store store : stores) {
            if (store.cheapestSmartphoneByModel(BIG_INT, modelName) != null) {
                return store;

            }
        }
        return null;
    }

    public void findAndPrintSmartphoneByManufacturer(String desiredManufacturer) {
        for (Store store : stores) {
            store.findSmartphoneByManufacturer(desiredManufacturer, store.getName(), store.getAddress());
        }
    }
}

