package com.company.model;

public class Store {
    private String name;
    private String address;
    private phone[] phones;

    public Store(String name, String address, phone[] phones) {
        this.name = name;
        this.address = address;
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void findSmartphoneByManufacturer(String manufacturer, String nameStore, String addressStore) {
        for (phone phone : phones) {
            if (phone.getManufacturer().equalsIgnoreCase(manufacturer)) {
                PrintInformation.printSmartphonesByManufacturer(phone,manufacturer, nameStore,addressStore);
            }
        }
    }

    public phone cheapestSmartphoneByModel(int lowerPrice, String modelName) {
        phone cheapestsmartphone = null;
        for (phone cheap : phones) {
            if (cheap.getPrice() < lowerPrice && cheap.getModel().equalsIgnoreCase(modelName)) {
                lowerPrice = cheap.getPrice();
                cheapestsmartphone = cheap;
            }
        }
        return cheapestsmartphone;
    }
}