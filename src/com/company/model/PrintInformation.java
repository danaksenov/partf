package com.company.model;

public final class PrintInformation {
    private PrintInformation() {
    }

    public static void printCheapestSmartphoneByModel(phone cheapSmartInShops, Store cheapestStore) {
        System.out.println("Смартфон, указанной Вами модели, по самой низкой цене: " + cheapSmartInShops.getManufacturer() + " " + cheapSmartInShops.getModel() + ", цена: " + cheapSmartInShops.getPrice() + ". Доступен в магазине: " + cheapestStore.getName() + ", адрес:" + cheapestStore.getAddress());
    }

    public static void printSmartphonesByManufacturer(phone phone, String manufacturer, String nameStore, String addressStore) {
        System.out.println((String.format("Ваш сматфон: %s %s", manufacturer, phone.getModel())) + ", по цене: " + phone.getPrice() + ", в магазине: " + nameStore + ", по адресу: " + addressStore);

    }
}


